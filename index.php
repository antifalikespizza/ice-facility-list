<!DOCTYPE html>
<head>
    <title>ICE information</title>
</head>
<body>
    <?
    $file = "ICE_Facility_List_11-06-2017-web.csv";
    
    
    $csv = array_map('str_getcsv', file($file));
    array_walk($csv, function(&$a) use ($csv) {
      $a = array_combine($csv[0], $a);
    });
    // array_shift($csv); # remove column header
    
    $stateCounts = array();
    
    foreach ( $csv as $key => $value ) {
        if ( $key == 0 ) { continue; }
        $state = $value["State"];
        // Count how many each state has
        if ( array_key_exists( $state, $stateCounts ) )
        {
            $stateCounts[ $state ]++;
        }
        else
        {
            $stateCounts[ $state ] = 1;
        }
        
    }
    
    ksort( $stateCounts );
    ?>
    
    <style type="text/css">
        table { width: 100%; overflow: scroll; }
        tr:nth-child(even) {background: #CCC}
        tr:nth-child(odd) {background: #FFF}
        
        td { border: solid 1px #aaa; }
        
        tr th { cursor: pointer; }
    </style>
    
    <h2>State counts:</h2>
    
    <table>
        <tr>
            <? foreach( $stateCounts as $state => $count ) { ?>
                <th><?=$state?></th>
            <? } ?>
        </tr>
        <tr>
            <? foreach( $stateCounts as $state => $count ) { ?>
                <td><?=$count?></td>
            <? } ?>
        </tr>
    </table>
    
    <h2>Download original file:</h2>
    <a href="https://immigrantjustice.org/ice-detention-facilities-november-2017">https://immigrantjustice.org/ice-detention-facilities-november-2017</a>
    
    <h2>Repository:</h2>
    <p><a href="https://bitbucket.org/%7B0e9d7842-2441-4f40-b3ed-d0398ab44b7c%7D/"></a></p>
    
    <h2>All data:</h2>  
    
    <p>
        There's lots of data, it can run kind of slow.
    </p> 
    
    <p>
        Click on a column header to sort by that column.
    </p>
    
    <table id="ice-data">
        <thead>
        <tr>
            <? foreach( $csv[0] as $k => $v ) { ?>
                <th role="columnheader"><?= $v?></th>
            <? } ?>
        </tr>
        </thead>
        
        <tbody>
        <? foreach( $csv as $key => $value ) { 
            if ( $key == 0 ) { continue; }
            ?>
            <tr>
                <? foreach( $value as $k => $v ) { ?>
                    <td><?=$v?></td>
                <? } ?>
            </tr>
        <? } ?>
        </tbody>
    </table>
    
    <script src='tablesort-5.0.2/src/tablesort.js'></script>

    <!-- Include sort types you need -->
    <script src='tablesort-5.0.2/src/sorts/tablesort.number.js'></script>
    <script src='tablesort-5.0.2/src/sorts/tablesort.date.js'></script>

    <script>
      new Tablesort(document.getElementById('ice-data'));
    </script>
</body>
